use lambda_runtime::{handler_fn, Context, Error};
use serde_json::{Value, json};
use rusoto_core::Region;
use rusoto_s3::{S3Client, S3, PutObjectRequest};

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(store_in_s3);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn store_in_s3(event: Value, _: Context) -> Result<Value, Error> {
    let s3_client = S3Client::new(Region::UsEast1);
    let bucket_name = "social_media_post";
    let file_content = json!({
        "user_id": event["user_id"],
        "content": event["content"],
        "sentiment": event["sentiment"],
        "timestamp": event["timestamp"]
    }).to_string();

    let put_request = PutObjectRequest {
        bucket: bucket_name.to_string(),
        key: format!("sentiment_results/{}.json", event["user_id"]),
        body: Some(file_content.into_bytes().into()),
        ..Default::default()
    };

    match s3_client.put_object(put_request).await {
        Ok(_) => Ok(json!({"status": "Data stored in S3"})),
        Err(e) => Err(Error::from(format!("Failed to store data in S3: {}", e))),
    }
}
