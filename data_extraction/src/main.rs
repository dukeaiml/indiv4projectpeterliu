use lambda_runtime::{handler_fn, Context, Error};
use serde_json::{Value, json};

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(extract_data);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn extract_data(event: Value, _: Context) -> Result<Value, Error> {
    let post_content = event["content"].as_str().unwrap_or_default();
    let user_id = event["user_id"].as_str().unwrap_or_default();
    let timestamp = event["timestamp"].as_str().unwrap_or_default();
    Ok(json!({"content": post_content, "user_id": user_id, "timestamp": timestamp}))
}
