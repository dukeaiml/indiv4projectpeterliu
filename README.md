# Step Function with Social Media Post Processing, Sentiment Analysis, and S3 Bucket Storage.

This repository contains code for 3 AWS Lambda functions (check data_extraction, sentiment_analysis and senttos3bucket) implemented in Rust, which integrates with AWS Step Functions to store (using simple wortd matching sentiment analysis) the sentiment of a social media post into an S3 bucket. This demonstrates how to orchestrate AWS services using a serverless architecture to automate workflows and enhance real-time notifications.

![image info](Screenshot_2024-04-24_144322.png)

## Overview

The 3 lambda functions are as follows. Data_extraction is designed to process the json file of the social media post and parse the relevant information. formatted, human-readable string. Sentiment_analysis does simple word matching to find sentiment. And finally senttos3bucket sends info to s3 bucket for creation. The Lambda function is then invoked by AWS Step Functions.


#### Functionality

1. **Initialization**: Sets up the AWS Lambda function with the necessary AWS SDK clients.
2. **Data Processing**: Aggregates the components of the post and formats them into a readable variables 
3. **Data Analysis**: Sentiment analysis on post and append to data.
4. **S3 Processing**: Adds bucket to S3.

## Setup and Deployment

### Requirements

- AWS CLI configured with appropriate permissions.
- Rust environment to build the Lambda function.
- AWS account and permissions to create and manage Lambda functions, Step Functions, and SNS topics.

### Deployment Steps

1. **Compile the Rust code**: Build the Lambda function using Cargo, ensuring it's compiled for the correct target that AWS Lambda supports (e.g., `x86_64-unknown-linux-gnu`).
2. **Package and Deploy Lambda**: Upload the compiled binary to AWS Lambda.

## Usage

After deployment, the state machine can be triggered with the AWS Management Console, or programmatically via AWS SDKs or CLI.

## Security and Permissions
Ensure IAM roles associated  have the necessary permissions for S3 access, invoking Lambda functions, running Step Functions. This architecture can be adapted to other uses with AWS services and serverless computing. The choice of Rust for the Lambda function offers performance advantages and memory safety.