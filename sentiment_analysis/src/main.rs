use lambda_runtime::{handler_fn, Context, Error};
use serde_json::{Value, json};

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(analyze_sentiment);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn analyze_sentiment(event: Value, _: Context) -> Result<Value, Error> {
    let content = event["content"].as_str().unwrap_or_default();
    let sentiment = if content.contains("happy") {"positive"} else {"neutral"};
    Ok(json!({"sentiment": sentiment, "content": content}))
}
